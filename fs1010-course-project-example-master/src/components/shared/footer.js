import React from 'react'
import { Container } from 'reactstrap'

const Footer = () => {
  return (
  <div className="bg-info mb-n1">
    <footer className="bg-info mb-0">
      <table className="table table-borderless text-white">
        <thead>
          <tr>
            <th scope="col"></th>
            <th scope="col">Social Media</th>
            <th scope="col">Locations</th>
            <th scope="col">Links</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row"></th>
            <td>Facebook</td>
            <td>Toronto</td>
            <td>My projects</td>
          </tr>
          <tr>
            <th scope="row"></th>
            <td>Instagram</td>
            <td>Ottawa</td>
            <td>My Resume</td>
          </tr>
          <tr>
            <th scope="row"></th>
            <td>GitHub</td>
            <td>London</td>
            <td>Contact Me</td>
          </tr>
        </tbody>
      </table>
    </footer>
    </div>
  );
};

export default Footer