import React from 'react'
import { Container, Row, Col, Button, CardBody, CardTitle, CardText, CardFooter, Card } from 'reactstrap'
import Bg from './assets/Bg.jpg'
import one from './assets/1.png'
import two from './assets/2.png'
import three from './assets/3.png'

const Home = () => {
    return(
        <Container>
            <Row className="my-5">
                <Col lg="7">
                    <img width="550px" height=""src={Bg} />
                </Col>
                <Col lg="5">
                    <h1 className="font-weight-light">ZOHREH PISHBIN</h1>
                    <p className="text-justify">Creative and analytical designer with a demonstrated history of working in the Marketing and IT field, now social media specialist and graphic designer at YLG. Skilled in digital marketing, Adobe Photoshop, Adobe Illustrator, Adobe After Effects, Motion Graphics, Teamwork, and Microsoft Office. Strong interpersonal skills with a Master's degree in business.</p>
                    <Button className="center-block " color="dark">My Resume</Button>
                </Col>
            </Row>
            <Row>
                <Col md="4" className="mb-5">
                    <Card>
                        <CardBody>
                            <CardTitle><h2></h2></CardTitle>
                            <CardBody><img width="220px" height=""src={one} /></CardBody>
                        </CardBody>
                        <CardFooter>
                            <Button color="dark" size="sm"> Projects</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col md="4" className="mb-5">
                    <Card>
                        <CardBody>
                            <CardTitle><h2></h2></CardTitle>
                            <CardBody><img width="220px" height=""src={two} /></CardBody>
                        </CardBody>
                        <CardFooter>
                            <Button color="dark" size="sm">Contact Me</Button>
                        </CardFooter>
                    </Card>
                </Col>
                <Col md="4" className="mb-5">
                    <Card>
                        <CardBody>
                            <CardTitle><h2></h2></CardTitle>
                            <CardBody><img width="220px" height=""src={three} /></CardBody>
                        </CardBody>
                        <CardFooter>
                            <Button color="dark" size="sm">Achievements</Button>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default Home