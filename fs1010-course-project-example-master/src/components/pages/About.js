import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { Link } from 'react-router-dom'
import three from './assets/zohreh.jpg';

const About = () => {
  return (
    <Container>
      <Row className="my-5">
        <Col lg="5">
          <img
            className="img-fluid rounded-circle "
            width="180px"
            src={three}
            alt=""
          />
          <h3 className="text-white" pb-4 pt-2>
            {" "}
            ZOHREH PISHBIN{" "}
          </h3>
        </Col>
        <Col lg="7">
          <h1 className="font-weight-light">About Me</h1>
          <p>
          Creative and analytical designer with a demonstrated history of working in the Marketing and IT field, now social media specialist and graphic designer at YLG. Skilled in digital marketing, Adobe Photoshop, Adobe Illustrator, Adobe After Effects, Motion Graphics, Teamwork, and Microsoft Office. Strong interpersonal skills with a Master's degree in business.{" "}
          </p>
          <Button color="dark" href="/contact">
            Contact Me
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default About;
